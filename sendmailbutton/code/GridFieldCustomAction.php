<?php
//
///**
// * This component provides a button for copying record.
// * First of all it dublicates record and then opens opens edit form {@link GridFieldDetailForm}.
// *
// * @package framework
// * @subpackage gridfield
// * @author Elvinas Liutkevičius <elvinas@unisolutions.eu>
// * @license BSD http://silverstripe.org/BSD-license
// */
//class GridFieldCustomAction implements GridField_ColumnProvider, GridField_ActionProvider
//{
//
//    public function augmentColumns($gridField, &$columns)
//    {
//        if (!in_array('Actions', $columns)) {
//            $columns[] = 'Actions';
//        }
//    }
//
//    public function getColumnAttributes($gridField, $record, $columnName)
//    {
//        return array('class' => 'col-buttons');
//    }
//
//    public function getColumnMetadata($gridField, $columnName)
//    {
//        if ($columnName == 'Actions') {
//            return array('title' => '');
//        }
//    }
//
//    public function getColumnsHandled($gridField)
//    {
//        return array('Actions');
//    }
//
//    public function getActions($gridField)
//    {
//        return array('sendmail');
//    }
//
//    public function getColumnContent($gridField, $record, $columnName)
//    {
//        if (!$record->canCreate()) {
//            return;
//        }
//        $field = GridField_FormAction::create($gridField, 'SendMail' . $record->ID, false, "sendmail",
//            array('RecordID' => $record->ID))
//            ->addExtraClass('gridfield-button-sendmail')
//            ->setAttribute('title', _t('GridAction.SendMail', "SendMail"))
//            ->setDescription(_t('GridAction.COPY_DESCRIPTION', 'SendMail'));
//
//        return $field->Field();
//    }
//
//    public function handleAction(GridField $gridField, $actionName, $arguments, $data)
//    {
//
//        $config = SiteConfig::current_site_config();
//        if ($actionName == 'sendmail') {
//            $item = $gridField->getList()->byID($arguments['RecordID']);
//            if (!$item) {
//                return;
//            }
//
//            $member = Member::get_by_id("Member", $arguments['RecordID']);
//
//            if (!$member->Email) {
//                throw new ValidationException(
//                    _t('GridFieldAction_SendMail.CreatePermissionsFailure', "No email"), 0);
//
//            } else {
//
//                $body = $config->MemberEmailBody;
//                $sender = $config->MemberEmailSender;
//                $email = new Email($sender, $member->Email, $config->MemberEmailSubject, $body);
//                $email->send();
//
//
//            }
//
//            if ($email->send()) {
//                $controller = Controller::curr();
//                $controller->response->addHeader('X-Status', rawurlencode(_t('GridFieldAction_SendMail.SendMailOK', 'Email sent.')));
//
//            }
//            else{
//                throw new ValidationException(
//                    _t('GridFieldAction_SendMail.SendMailError', "Error"), 0);
//            }
//
//        }
//    }
//}
//
//
