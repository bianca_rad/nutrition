/*!
* Themerella
*
* (c) Copyright themerella.com
*
* @version 1.0.0
* @author  Themerella
*/


(function ($) {
    'use strict';
})(jQuery);

$(document).ready(function () {

    $.ajax({
        url: 'http://troy.welldev.ro/commerce-online/commerce/getdiets',
        dataType: 'json',
        crossDomain: true,
        success: function (json) {
            $.each(json, function (i, value) {
                $('#diet').append($('<option >').text(value.name).attr('value', value.id));
            });

        }
    });
    $.ajax({
        url: 'http://troy.welldev.ro/commerce-online/commerce/getingredients',
        type: "GET",
        dataType: "json",
        success: function (json) {
            r = "";
            $.each(json.meats, function (i, value) {
                r += '<div class="ckbox-group"><input type="checkbox" id="meat' + value.id + '" name="meat[]" value="' + value.id + '"><label for="meat' + value.id + '">' + value.name + '</label></div>';
            });
            document.getElementById("meats-wrap").innerHTML = r;
            r = "";
            $.each(json.veges, function (i, value) {
                r += '<div class="ckbox-group"><input type="checkbox" id="vegetable' + value.id + '" name="vegetable[]" value="' + value.id + '"><label for="vegetable' + value.id + '">' + value.name + '</label></div>';
            });
            document.getElementById("veges-wrap").innerHTML = r;

            r = "";
            $.each(json.fruits, function (i, value) {
                r += '<div class="ckbox-group"><input type="checkbox" id="fruit' + value.id + '" name="fruit[]" value="' + value.id + '"><label for="fruit' + value.id + '">' + value.name + '</label></div>';
            });
            document.getElementById("fruits-wrap").innerHTML = r;

            r = "";
            $.each(json.dairies, function (i, value) {
                r += '<div class="ckbox-group"><input type="checkbox" id="dairy' + value.id + '" name="dairy[]" value="' + value.id + '"><label for="dairy' + value.id + '">' + value.name + '</label></div>';
            });
            document.getElementById("dairies-wrap").innerHTML = r;
            r = "";
            $.each(json.others, function (i, value) {
                r += '<div class="ckbox-group"><input type="checkbox" id="others' + value.id + '" name="others[]" value="' + value.id + '"><label for="others' + value.id + '">' + value.name + '</label></div>';
            });
            document.getElementById("others-wrap").innerHTML = r;



            //---------------------select just 3-----------------------


            $(":checkbox[name='meat[]']").on('click', function () {
                var checkedBoxlength = $(':checkbox[name=\'meat[]\']:checked').length;
                if (checkedBoxlength > 3) {
                    $('.ckbox-alert-meat').show();
                    return false;
                }
                else {
                    $('.ckbox-alert-meat').hide();
                }
            });

            $(':checkbox[name="vegetable[]"]').on('click', function () {

                var checkedBoxlength = $(':checkbox[name="vegetable[]"]:checked').length;
                if (checkedBoxlength > 3) {
                    $('.ckbox-alert-vegetable').show();
                    return false;
                }
                else {
                    $('.ckbox-alert-vegetable').hide();
                }
            });



            $(':checkbox[name="dairy[]"]').on('click', function () {

                var checkedBoxlength = $(':checkbox[name="dairy[]"]:checked').length;
                if (checkedBoxlength > 3) {
                    $('.ckbox-alert-dairy').show();
                    return false;
                }
                else {
                    $('.ckbox-alert-dairy').hide();
                }
            });

            $(':checkbox[name="fruit[]"]').on('click', function () {

                var checkedBoxlength = $(':checkbox[name="fruit[]"]:checked').length;
                if (checkedBoxlength > 3) {
                    $('.ckbox-alert-fruit').show();
                    return false;
                }
                else {
                    $('.ckbox-alert-fruit').hide();
                }
            });

            $(':checkbox[name="others[]"]').on('click', function () {

                var checkedBoxlength = $(':checkbox[name="others[]"]:checked').length;
                if (checkedBoxlength > 3) {
                    $('.ckbox-alert-others').show();
                    return false;
                }
                else {
                    $('.ckbox-alert-others').hide();
                }
            });

            $(".alert-close").on('click', function () {
                $('.checkbox-alert').hide();
            })
        }
    });


    //---------------------------------------------------------------------------------
    var $form = $('#profileForm');

    if ($form.length) {

        $form.validate({
            // Specify validation rules
            rules: {

                'weight': {
                    required: true,
                    digits: true,
                    range: [35, 250]
                },
                'height': {
                    required: true,
                    digits: true,
                    range: [120, 250]
                },
                'age': {
                    required: true,
                    digits: true,
                    range: [8, 75]
                },
                'email': {
                    required: true,
                    email: true
                },

                'diet':  {
                    required: true
                }, 'privacy':  {
                    required: true
                },
                'activity': "required",
                'sex': "required",

            },
            // Specify validation error messages
            messages: {
                'weight': {required: weightRequired, digits: validNumberRequired,  range: rangeWeightRequired},
                'height': {required: heightRequired, digits: validNumberRequired, range: rangeHeightRequired},
                'age': {required: ageRequired, digits: validNumberRequired, range: rangeAgeRequired},
                'diet': {required: dietRequired},
                'privacy': {required: required},
                'activity': activityRequired,
                'sex': sexRequired,
                'email': {
                    required: emailRequired,
                    email: validEmailRequired
                }

            }
        })
    }


    var $contactform = $('#contactForm');

    if ($contactform.length) {

        $contactform.validate({
            // Specify validation rules
            rules: {

                'email': {
                    required: true,
                    email: true
                },

                'subject': "required",
                'name': "required",
                'message': "required",
                'privacy': "required"

            },
            // Specify validation error messages
            messages: {

                'subject': subjectRequired,
                'name': nameRequired,
                'message': messageRequired,
                'privacy': required,
                'email': {
                    required: emailRequired,
                    email: validEmailRequired
                }

            }
        })
    }


    var $cardpaymentform = $('#frmPaymentRedirect');

    if ($cardpaymentform.length) {

        $cardpaymentform.validate({
            // Specify validation rules
            rules: {


                'billing_type': "required",
                'billing_first_name': "required",
                'billing_last_name': "required",
                'billing_address': "required",
                'billing_email': {
                    required: true,
                    email: true
                },
                'billing_mobile_phone': "required",
                'privacy': "required",


            },
            // Specify validation error messages
            messages: {


                'billing_type': subjectRequired,
                'billing_first_name': subjectRequired,
                'billing_last_name': subjectRequired,
                'billing_address': subjectRequired,
                'billing_email': {
                    required: emailRequired,
                    email: validEmailRequired
                },
                'billing_mobile_phone': subjectRequired,
                'privacy': required,


            }
        })
    }




//    ----------------------------------------------------------

    $('a[href^="#"]').click(function () {

        var the_id = $(this).attr("href");

        $('html, body').animate({
            scrollTop: $(the_id).offset().top
        }, 'slow');

        return false;
    });

});


jQuery(document).ready(function($) {

    $.cookieBar({
        message : $('.cookies').data('text'),
        acceptButton : true,
        acceptText : $('.cookies').data('accept'),
        declineButton : false,
        declineText : 'Disable Cookies',
        policyButton : false,
        policyText : 'Cookie Policy',
        policyURL : '/privacy/',
        autoEnable : true,
        acceptOnContinue : false,
        expireDays : 365,
        forceShow : false,
        effect : 'fade',
        element : 'body',
        append : false,
        fixed : true,
        bottom : true,
        zindex : '9999999',
        redirect : '/',
        domain : 'http://nutritie.welldev.ro/',
        referrer : 'http://nutritie.welldev.ro/'
    });

});