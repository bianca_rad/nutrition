<?php

use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\View\SSViewer;
use SilverStripe\View\Requirements;

class PageController extends ContentController
{
    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * [
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * ];
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = [];

    protected function init()
    {
        parent::init();

        $printStylesheets = array(
            "themes/simple/css/bootstrap.css",
            "themes/simple/css/theme-vendors.css",
            "themes/simple/css/theme.css",
            "themes/simple/css/custom.css",
            "resources/themes/simple/vendors/themepunch/settings.css",
        );

        Requirements::combine_files('style.css', $printStylesheets);

        Requirements::combine_files(
            'apps.js',
            array(
                "resources/themes/simple/javascript/core.min.js",
                "resources/themes/simple/javascript/theme.vendors.min.js",
                "resources/themes/simple/javascript/script.js",
                "resources/themes/simple/javascript/theme.min.js",
                "resources/themes/simple/javascript/cookiebar.js",
                "resources/themes/simple/javascript/validate.js",
                "resources/themes/simple/javascript/custom.js",
                "resources/themes/simple/vendors/themepunch/jquery.themepunch.tools.min.js",
                "resources/themes/simple/vendors/themepunch/jquery.themepunch.revolution.min.js",
                "resources/themes/simple/vendors/themepunch/extensions/revolution.extension.slideanims.min.js",
                "resources/themes/simple/vendors/themepunch/extensions/revolution.extension.actions.min.js",
                "resources/themes/simple/vendors/themepunch/extensions/revolution.extension.layeranimation.min.js",
                "resources/themes/simple/vendors/themepunch/extensions/revolution.extension.parallax.min.js",
                "themes/simple/javascript/rev-slider-init-main.js"

            )
        );
    }
}

