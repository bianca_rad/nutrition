<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 2:12 PM
 */

namespace SilverStripe\Nutrition;

use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\AssetAdmin\Forms\UploadField;


class AboutBox extends DataObject
{
    private static $table_name = 'AboutBox';


    private static $db = [
        'Title' => 'Varchar(255)',
        'Text' => 'HTMLText',
        'Icon' => 'Varchar(255)',


    ];

    private static $has_one = [

        'AboutPage' => AboutPage::class,
    ];

    private static $owns = [

    ];
    private static $summary_fields = [
        "Title" => "Title",
        "Icon" => "Icon"
    ];
    private static $api_access = true;


    public function getCMSFields()
    {


        $fields = FieldList::create(
            TextField::create('Title'),
            HTMLEditorField::create('Text'),
            TextField::create('Icon')


        );


        return $fields;
    }
}