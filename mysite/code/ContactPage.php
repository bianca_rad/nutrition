<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 1:17 PM
 */
namespace SilverStripe\Nutrition;
use Page;
use PageController;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use View;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Control\Email\Email;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;


class ContactPage extends Page
{
    private static $table_name = 'ContactPage';
    private static $db = [
        'Subtitle' => 'Varchar(255)',
        'MapText' => 'HTMLText',
        'Address' => 'Varchar(255)',
        'Phone' => 'Varchar(255)',
        'Email' => 'Varchar(255)',
        'SenderEmail' => 'Varchar(255)',
        'ReceiverEmail' => 'Varchar(255)',
        'EmailSubject' => 'Varchar(255)',
    ];
    private static $has_one = [
        'Background' => Image::class,

    ];
    private static $owns = [
        'Background',"ContactFormEntry"
    ];
    private static $has_many = [
        'ContactFormEntries' => ContactFormEntry::class,

    ];
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields ->addFieldToTab("Root.Main",    TextField::create('Subtitle'));
        $fields ->addFieldToTab("Root.Main",    HTMLEditorField::create('MapText'));
        $fields ->addFieldToTab("Root.Main",    TextField::create('Address'));
        $fields ->addFieldToTab("Root.Main",    TextField::create('Phone'));
        $fields ->addFieldToTab("Root.Main",    TextField::create('Email'));
        $fields ->addFieldToTab('Root.Main', UploadField::create('Background'));
        $fields ->addFieldToTab("Root.Form",    TextField::create('SenderEmail'));
        $fields ->addFieldToTab("Root.Form",    TextField::create('ReceiverEmail'));
        $fields ->addFieldToTab("Root.Form",    TextField::create('EmailSubject'));
        $fields->addFieldToTab('Root.Form', GridField::create(
            'ContactFormEntries',
            'ContactFormEntries on this page',
            $this->ContactFormEntries(),
            GridFieldConfig_RecordEditor::create()
        ));

//
        return $fields;
    }


}

class ContactPageController extends PageController
{
    private static $allowed_actions = [
        'sendContactForm'
    ];
    
    public function sendContactForm($data)
    {
       // $config = SiteConfig::current_site_config();
        $subject = $data['subject'];
        $name = $data['name'];
        $email = $data['email'];
        $message = $data['message'];
        $privacy = $data['privacy'];

        if (!$subject || !$email || !$name || !$message || !$privacy ) {
            //  $form->addErrorMessage('YourMessage', 'Your message is too short', 'bad');
            return $this->redirectBack();
        }

        
        $form = ContactFormEntry::create();
        $form->Name = $name;
        $form->Subject = $subject;
        $form->Email = $email;
        $form->Message = $message;
        $form->ContactPageID = $this->ID;
        $form->write();

        
        
        $address = $this->ReceiverEmail;
        $senderaddress = $this->SenderEmail;

        $e_subject = $this->EmailSubject;


        $body = _t("ContactForm.PaidBy", "Nume: "). ":  <strong>" . $name. "</strong><br/>";
        $body .= _t("ContactForm.FirstName", "Subiect: ") . ":  <strong>" . $subject. "</strong><br/>";
        $body .= _t("ContactForm.Surname", "Email: ") . ":  <strong>" . $email . "</strong><br/>";
        $body .= _t("ContactForm.Address", "Mesaj: ") . ":  <strong>" . $message . "</strong><br/>";



        $form_email = new Email($senderaddress, $address, $e_subject, $body);
       if( $form_email->send()){
           return $this->redirect('/multumim');
       }


        // $form->sessionMessage('Thanks for your submit!', 'good');


        return $this->redirect('/ok');
    }
	
}