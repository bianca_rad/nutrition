<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\TextField;

class SiteConfig extends DataExtension
{

    private static $db = [
        'BillNumber' => 'Varchar(50)',
        'PriceMenu' => 'Varchar(50)',
        'SenderEmail' => 'Varchar(50)',
    ];

    public function updateCMSFields(FieldList $fields)
    {

        $fields->addFieldToTab('Root.Menu', TextField::create('BillNumber'));
        $fields->addFieldToTab('Root.Menu', TextField::create('PriceMenu'));
        $fields->addFieldToTab('Root.Menu', TextField::create('SenderEmail'));
    }
}