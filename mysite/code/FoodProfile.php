<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 2:12 PM
 */
namespace SilverStripe\Nutrition;

use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\AssetAdmin\Forms\UploadField;


class FoodProfile extends DataObject
{
    private static $table_name = 'FoodProfile';

    private static $db = [
        'Weight' => 'Int',
        'Height' => 'Int',
        'Email' => 'Varchar',
        'Age' => 'Int',
        'Activity' => 'Int',
        'Sex' => 'Int',
        'Diet' => 'Varchar(500)',
        'Message' => 'Text',
        'Meats' => 'Varchar(500)',
        'Vegetables' => 'Varchar(500)',
        'Fruits' => 'Varchar(500)',
        'Dairys' => 'Varchar(500)',
        'Others' => 'Varchar(500)',
        'Error' => 'Varchar(500)'
    ];

    private static $has_one = [

        'HomePage' => HomePage::class,
    ];

    private static $owns = [

    ];
    private static $summary_fields = [
        'Weight' => 'Weight',
        'Height' => 'Height',
        'Age' => 'Age',
        'Email' => 'Email',
        'Message' => 'Message',
        'Error' => 'Error',
    ];
    private static $api_access = true;


//    public function canView($member = null)
//    {
//        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
//    }
//
//    public function canEdit($member = null)
//    {
//        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
//    }
//
//    public function canDelete($member = null)
//    {
//        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
//    }
//
//    public function canCreate($member = null)
//    {
//        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
//    }

    public function getCMSFields()
    {

        $activity = array( '1' => 'Sedentar', '2' => 'Activ', '3'=> 'Foarte Activ' );
        $sex = array( '1' => 'Masculin', '2' => 'Feminin' );

        $fields = FieldList::create(
            TextField::create('Weight'),
            TextField::create('Height'),
            TextField::create('Email'),
            TextField::create('Age'),
            DropdownField::create('Activity','Activity',$activity),
            DropdownField::create('Sex','Sex',$sex),
            TextField::create('Diet'),
            TextField::create('Meats'),
            TextField::create('Vegetables'),
            TextField::create('Fruits'),
            TextField::create('Dairys'),
            TextField::create('Others'),
            TextareaField::create('Message'),
            TextField::create('Error')

        );


        return $fields;
    }
}