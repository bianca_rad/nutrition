<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 1:17 PM
 */

namespace SilverStripe\Nutrition;

//use SilverStripe\Control\Director;
use Page;
use PageController;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
use View;
use SilverStripe\Assets\File;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Control\Email\Email;


class MenuTestPage extends Page
{
    private static $table_name = 'MenuTestPage';
    private static $db = [


    ];
    private static $has_one = [

    ];
    private static $owns = [

    ];
    private static $has_many = [

    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        return $fields;
    }


}

class MenuTestPageController extends PageController
{


    private static $allowed_actions = [
        'sendProfileForm'
    ];

    public function index()
    {
        if (isset($_GET["err"]) && $_GET["err"]) {
            return $this->customise(["Err" => $_GET["err"]]);
        }
        return $this;
    }

    public function gender()
    {

        $arr = array('1' => 'Masculin', '2' => 'Feminin');
        $arrlist = new ArrayList();
        foreach ($arr as $key => $value) {
            $arrlist->push(
                new ArrayData(array('id' => $key, 'gender' => $value))
            );
        }
        return $arrlist;

    }

    public function activity()
    {

        $arr = array('1' => 'Sedentar', '2' => 'Activ', '3' => 'Foarte Activ');
        $arrlist = new ArrayList();
        foreach ($arr as $key => $value) {
            $arrlist->push(
                new ArrayData(array('id' => $key, 'activity' => $value))
            );
        }
        return $arrlist;

    }

    public function diet()
    {

        $arr = array('1' => 'Vegan', '2' => 'Carnivor', '3' => 'Ovo-lacto-vegetarian', '4' => 'Ovo-vegetarian');
        $arrlist = new ArrayList();
        foreach ($arr as $key => $value) {
            $arrlist->push(
                new ArrayData(array('id' => $key, 'diet' => $value))
            );
        }
        return $arrlist;

    }


    public function sendProfileForm($data)
    {


        $weight = $data['weight'];
        $height = $data['height'];
        $age = $data['age'];
        $activity = $data['activity'];
        $diet = $data['diet'];
        $sex = $data['sex'];
        $email = $data['email'];
        $message = $data['message'];


        $meats = "";
        $meats_ar = array();
        if ($data['meat']) {
            foreach ($data['meat'] as $selected) {
                $meats .= $selected . " ";
                array_push($meats_ar, $selected);
            }
        }


        $fruits = "";
        $fruits_ar = array();
        if ($data['fruit']) {
            foreach ($data['fruit'] as $selected) {
                $fruits .= $selected . " ";
                array_push($fruits_ar, $selected);
            }
        }


        $vegetables = "";
        $vegetables_ar = array();
        if ($data['vegetable']) {
            foreach ($data['vegetable'] as $selected) {
                $vegetables .= $selected . " ";
                array_push($vegetables_ar, $selected);
            }
        }

        $dairys = "";
        $dairys_ar = array();
        if ($data['dairy']) {
            foreach ($data['dairy'] as $selected) {
                $dairys .= $selected . " ";
                array_push($dairys_ar, $selected);
            }
        }

        $others = "";
        $others_ar = array();
        if ($data['others']) {
            foreach ($data['others'] as $selected) {
                $others .= $selected . " ";
                array_push($others_ar, $selected);
            }
        }


        if (!$age || !$email || !$weight || !$height || !$activity || !$diet || !$sex) {
            //  $form->addErrorMessage('YourMessage', 'Completati toate campurile', 'bad');
            return $this->redirectBack();
        }


        $profile = FoodProfile::create();
        $profile->Weight = $weight;
        $profile->Height = $height;
        $profile->Age = $age;
        $profile->Activity = $activity;
        $profile->Diet = $diet;
        $profile->Sex = $sex;
        $profile->Email = $email;
        $profile->Message = $message;

        $profile->Meats = $meats;
        $profile->Vegetables = $vegetables;
        $profile->Fruits = $fruits;
        $profile->Dairys = $dairys;
        $profile->Others = $others;


        $profile->HomePageID = 1;
        $profile->write();

        //  session_start();
        //  $_SESSION["foodProfID"] = $profile->ID;


//        -----------------------------------------------------------
//        -------------------------payment---------------------------
//        -----------------------------------------------------------


        $service_url = 'http://troy.welldev.ro/commerce-online/commerce/generateMenu';
        $curl = curl_init($service_url);


        $curl_post_data = array(
           //  'weight' => '10',
            'weight' => $weight,
            'height' => $height,
            'age' => $age,
            'sex' => $sex,
            'activity' => $activity,
            'diet' => $diet,
            'meats' => $meats_ar,
            'veges' => $vegetables_ar,
            'fruits' => $fruits_ar,
            'dairies' => $dairys_ar,
            'others' => $others_ar,
            'requestIdentifier' => $profile->ID,
            'email' => $email,
        );


        $data_string = json_encode($curl_post_data);


        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $curl_response = curl_exec($curl);
//die(print_r(json_decode($curl_response)->errormessage->errorCode));
        if ($curl_response == false || json_decode($curl_response)->errormessage->errorCode) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            $email = new Email("bbiiaa2594@gmail.com", "bbiiaa2594@gmail.com", "menu curl error-nutritie", var_export($info));
            $email->send();
            return $this->redirect('/menu-test/?err='.json_decode($curl_response)->errormessage->errorCode);
        } else {


            $filename = "token" . $profile->ID; //replace with payment token
            $path = $_SERVER["DOCUMENT_ROOT"] . "/public/assets/menus/" . $filename . ".pdf";
            file_put_contents($path, $curl_response);

            $file = File::create();
            $file->setFromLocalFile($path, "/menus/" . $filename . ".pdf");
            $file->Filename = $filename . ".pdf";
            $file->FileFilename = "menus/" . $filename . ".pdf";
            $file->ParentID = 118;
            $file->write();
            $file->doPublish();

            curl_close($curl);



            $email = new Email("bbiiaa2594@gmail.com", "bbiiaa2594@gmail.com", "menu", "menu");
          //   $email->addCC("iova.anca@gmail.com");
            //  $email->addCC("nutritionistelena@yahoo.com");
            // $email->addBCC("dorin.iova@welltech.ro");

            $email->addAttachment($path);
            $email->send();
            return $this->redirect('/ok');
        }


    }
}