<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 1:17 PM
 */

namespace SilverStripe\Nutrition;

use Page;
use PageController;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use View;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\TextField;


class AboutPage extends Page
{
    private static $table_name = 'AboutPage';
    private static $db = [
        'Subtitle' => 'HTMLText',
    ];
    private static $has_one = [
        'Background' => Image::class,
        'SideImage1' => Image::class,
        'SideImage2' => Image::class,
        'SideImage3' => Image::class,
    ];
    private static $owns = [
        'Background',"SideImage1",'SideImage2','SideImage3',"AboutBox"
    ];
    private static $has_many = [
        'AboutBoxes' => AboutBox::class,

    ];
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();


        $fields->addFieldToTab(
            'Root.Main',
            HTMLEditorField::create('Subtitle', 'Subtitlu')
        );
        $fields->addFieldToTab('Root.Main', UploadField::create('Background'));
        $fields->addFieldToTab('Root.Main', UploadField::create('SideImage1'));
        $fields->addFieldToTab('Root.Main', UploadField::create('SideImage2'));
        $fields->addFieldToTab('Root.Main', UploadField::create('SideImage3'));
        $fields->addFieldToTab('Root.Boxes', GridField::create(
            'AboutBoxes',
            'AboutBoxes on this page',
            $this->AboutBoxes(),
            GridFieldConfig_RecordEditor::create()
        ));
        return $fields;
    }


}

class AboutPageController extends PageController
{


}