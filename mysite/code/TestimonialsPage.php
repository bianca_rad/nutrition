<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 1:17 PM
 */

namespace SilverStripe\Nutrition;



use Page;
use PageController;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use View;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\TextField;



class TestimonialsPage extends Page
{
    private static $table_name = 'TestimonialsPage';
    private static $db = [
        'Subtitle' => 'HTMLText',
    ];
    private static $has_one = [
        'Background' => Image::class,
    ];
    private static $has_many = [
        'Testimonials' => Testimonial::class,

    ];
    private static $owns = [
'Background'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldToTab(
            'Root.Main',
            HTMLEditorField::create('Subtitle', 'Subtitlu')
        );
        $fields->addFieldToTab('Root.Main', UploadField::create('Background'));
        $fields->addFieldToTab('Root.Testimonials', GridField::create(
            'Testimonials',
            'Testimonials on this page',
            $this->Testimonials(),
            GridFieldConfig_RecordEditor::create()
        ));
        return $fields;
    }


}

class TestimonialsPageController extends PageController
{


}