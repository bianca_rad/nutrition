<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 2:12 PM
 */
namespace SilverStripe\Nutrition;

use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\AssetAdmin\Forms\UploadField;


class ContactFormEntry extends DataObject
{
    private static $table_name = 'ContactFormEntry';

    private static $db = [
        'Name' => 'Varchar(500)',
        'Email' => 'Varchar(500)',
        'Subject' => 'Varchar(500)',
        'Message' => 'Text',

    ];

    private static $has_one = [

        'ContactPage' => ContactPage::class,
    ];

    private static $owns = [

    ];
    private static $summary_fields = [
        'Name' => 'Name',
        'Email' => 'Email',
        'Subject' => 'Subject',
        'Message' => 'Message',
    ];
    private static $api_access = true;


//    public function canView($member = null)
//    {
//        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
//    }
//
//    public function canEdit($member = null)
//    {
//        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
//    }
//
//    public function canDelete($member = null)
//    {
//        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
//    }
//
//    public function canCreate($member = null)
//    {
//        return Permission::check('CMS_ACCESS_MyAdmin', 'any', $member);
//    }

    public function getCMSFields()
    {



        $fields = FieldList::create(
            TextField::create('Name'),
            TextField::create('Subject'),
            TextField::create('Email'),
            TextareaField::create('Message')

        );


        return $fields;
    }
}