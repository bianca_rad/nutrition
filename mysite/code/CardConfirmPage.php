<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 1:17 PM
 */

namespace SilverStripe\Nutrition;

use Mobilpay_Payment_Request_Card;
use Mobilpay_Payment_Request_Abstract;
use Page;
use PageController;
use View;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Control\Director;
use SilverStripe\ORM\ArrayList;
use Mobilpay_Payment_Request_Sms;
use Mobilpay_Payment_Invoice;
use Mobilpay_Payment_Address;

//use SilverStripe\ORM\DataList;
use SilverStripe\Nutrition\Member;
use SilverStripe\Nutrition\Client;
use SilverStripe\FPDF\FPDF;
use SilverStripe\Control\Email\Email;
use Silverstripe\SiteConfig\SiteConfig;
use SilverStripe\Assets\File;

//use SilverStripe\ORM\ArrayList;
//use SilverStripe\ORM\ArrayList;

use SilverStripe\View\ArrayData;

//use SilverStripe\CMS\Controllers\ContentController;

class CardConfirmPage extends Page
{
    private static $table_name = 'CardConfirmPage';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        return $fields;
    }


}

class CardConfirmPageController extends PageController
{
    private static $allowed_actions = [
        'sendGenerateMenu', "sendGenerateBill"
    ];

    public function index()
    {

//        nutritionist.elena
//        sdf3245hj567jklj98


        require(Director::baseFolder() . '/PHP_CARD/Mobilpay/Payment/Request/Abstract.php');
        require(Director::baseFolder() . '/PHP_CARD/Mobilpay/Payment/Request/Card.php');
        require(Director::baseFolder() . '/PHP_CARD/Mobilpay/Payment/Request/Notify.php');
        require(Director::baseFolder() . '/PHP_CARD/Mobilpay/Payment/Invoice.php');
        require(Director::baseFolder() . '/PHP_CARD/Mobilpay/Payment/Address.php');


        $errorCode = 0;
        $errorType = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_NONE;
        $errorMessage = '';

        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'post') == 0) {
            if (isset($_POST['env_key']) && isset($_POST['data'])) {
                #calea catre cheia privata
                #cheia privata este generata de mobilpay, accesibil in Admin -> Conturi de comerciant -> Detalii -> Setari securitate
                $privateKeyFilePath = Director::baseFolder() . '/PHP_CARD/sandbox.TPVS-Q9JN-WFWR-NP2F-U1A2private.key';

                try {
                    $objPmReq = Mobilpay_Payment_Request_Abstract::factoryFromEncrypted($_POST['env_key'], $_POST['data'], $privateKeyFilePath);
                    #uncomment the line below in order to see the content of the request
                    //print_r($objPmReq);
                    $errorCode = $objPmReq->objPmNotify->errorCode;
                    // action = status only if the associated error code is zero
                    if ($errorCode == "0") {
                        switch ($objPmReq->objPmNotify->action) {
                            #orice action este insotit de un cod de eroare si de un mesaj de eroare. Acestea pot fi citite folosind $cod_eroare = $objPmReq->objPmNotify->errorCode; respectiv $mesaj_eroare = $objPmReq->objPmNotify->errorMessage;
                            #pentru a identifica ID-ul comenzii pentru care primim rezultatul platii folosim $id_comanda = $objPmReq->orderId;
                            case 'confirmed':
                                #cand action este confirmed avem certitudinea ca banii au plecat din contul posesorului de card si facem update al starii comenzii si livrarea produsului
                                //update DB, SET status = "confirmed/captured"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'confirmed_pending':
                                #cand action este confirmed_pending inseamna ca tranzactia este in curs de verificare antifrauda. Nu facem livrare/expediere. In urma trecerii de aceasta verificare se va primi o noua notificare pentru o actiune de confirmare sau anulare.
                                //update DB, SET status = "pending"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'paid_pending':
                                #cand action este paid_pending inseamna ca tranzactia este in curs de verificare. Nu facem livrare/expediere. In urma trecerii de aceasta verificare se va primi o noua notificare pentru o actiune de confirmare sau anulare.
                                //update DB, SET status = "pending"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'paid':
                                #cand action este paid inseamna ca tranzactia este in curs de procesare. Nu facem livrare/expediere. In urma trecerii de aceasta procesare se va primi o noua notificare pentru o actiune de confirmare sau anulare.
                                //update DB, SET status = "open/preauthorized"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'canceled':
                                #cand action este canceled inseamna ca tranzactia este anulata. Nu facem livrare/expediere.
                                //update DB, SET status = "canceled"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'credit':
                                #cand action este credit inseamna ca banii sunt returnati posesorului de card. Daca s-a facut deja livrare, aceasta trebuie oprita sau facut un reverse.
                                //update DB, SET status = "refunded"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            default:
                                $errorType = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
                                $errorCode = Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_ACTION;
                                $errorMessage = 'mobilpay_refference_action paramaters is invalid';
                                break;
                        }
                    } else {
                        //update DB, SET status = "rejected"
                        $errorMessage = $objPmReq->objPmNotify->errorMessage;
                    }
                } catch (Exception $e) {
                    $errorType = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_TEMPORARY;
                    $errorCode = $e->getCode();
                    $errorMessage = $e->getMessage();
                }
            } else {
                $errorType = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
                $errorCode = Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_POST_PARAMETERS;
                $errorMessage = 'mobilpay.ro posted invalid parameters';
            }
        } else {
            $errorType = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
            $errorCode = Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_POST_METHOD;
            $errorMessage = 'invalid request metod for payment confirmation';
        }


        $client = Client::get()->filter(["OrderID" => $objPmReq->orderId])->first();
        if ($errorCode == 0) {

            $client->ErrorCode = "no error";
            $client->ErrorType = "";
            $client->ErrorMessage = "";
            $client->Paid = 1;

            $menu = $client->FoodProfile();

////------------------------------------------------------------------------------------------------------------------------------------------
            $config = SiteConfig::current_site_config();
            ob_start();
            require($_SERVER["DOCUMENT_ROOT"] . '/fpdf/fpdf.php');

            $nr = ((int)date('Y') - 2017) * 10000 + (int)$config->BillNumber + 1;
            $config->BillNumber = $config->BillNumber + 1;
            $config->write();

            $pdf = new FPDF();
            $pdf->AddPage();

            $pdf->Image('http://nutritie.welldev.ro/public/factura-nutritie.jpg', 0, 0, 210);
            $pdf->SetFont('Arial', '', 10);

            $pdf->SetXY(42, 21.5);
            $pdf->Cell(0, 0, $nr);


            $pdf->SetXY(38, 26.5);
            $pdf->Cell(0, 0, date("d/m/Y", strtotime($client->Created)));

            $pdf->SetXY(74, 26.5);
            $pdf->Cell(0, 0, date("d/m/Y", strtotime($client->Created)));

            $pdf->SetFont('Arial', '', 11);

            $pdf->SetXY(110, 44);
            $pdf->Cell(0, 0, $client->BillingFirstName . " " . $client->BillingLastName);

            $pdf->SetXY(110, 49);
            $pdf->Cell(0, 0, $client->BillingAddress);

            $pdf->SetXY(110, 54);
            $pdf->Cell(0, 0, "Telefon: " . $client->BillingPhone);

            $pdf->SetXY(110, 59);
            $pdf->Cell(0, 0, "Email: " . $client->BillingEmail);


            $pdf->SetFont('Arial', '', 12);


            $pdf->SetXY(140, 75);
            $pdf->Cell(0, 0, $config->PriceMenu);

            $pdf->SetXY(167, 75);
            $pdf->Cell(0, 0, $config->PriceMenu);


            $pdf->SetXY(167, 163);
            $pdf->Cell(0, 0, $config->PriceMenu);

            $pdf->SetXY(187, 177);
            $pdf->Cell(0, 0, $config->PriceMenu);


            $name = $client->OrderID . "bill";
            $path = $_SERVER["DOCUMENT_ROOT"] . "/facturi/" . $name . ".pdf";
            $pdf->Output($path, 'F');


            $file = File::create();

            $file->setFromLocalFile($path, "/facturi/" . $name . ".pdf");
            $file->Filename = $name . ".pdf";
            $file->FileFilename = "facturi/" . $name . ".pdf";
            $file->ParentID = 91;
            $file->write();
            $file->doPublish();

            $client->Document = $file;


//send to client and admin address

            $to = $client->BillingEmail;
            $message = '';

            $email = new Email($config->SenderEmail, $to, 'Factura Cabinet Nutritie Elena Bodea – Meniu', $message);
            $email->addAttachment($path);
            $email->send();


            ob_end_flush();


////------------------------------------------------------------------------------------------------------------------------------------------


            // Send the menu

            $filename1 = "menu" . $client->FoodProfile()->ID; //replace with payment token
            $path1 = $_SERVER["DOCUMENT_ROOT"] . "/public/assets/menus/" . $filename1 . ".pdf";


            $body="Buna ziua, <br><br>

In atasament aveti meniul comandat de dumneavoastra.<br><br>

Nu uita: Schimbarea aduce: Incredere, Bucurie, Sanatate.<br><br>

Acest program  te va ajuta sa-ti schimbi obiceiurile alimentare printr-un regim alimentar echilibrat si variat, pana cand alimentatia normala va deveni  rutina. Aceasta este cheia pierderii eficiente in greutate. Regimurile create de mine includ numai produsele indicate de tine, in cantitatile potrivite, prin care iti vei atinge scopul dorit. Fiecare regim este strict individual.
<br><br>
Iti doresc: SUCCES!<br><br>

Spune acum: DA! VREAU! EU POT! EU FAC!<br><br>

Dupa o luna va trebui sa comanzi alt meniu deoarece scazand in greutate, s-ar putea sa se fi schimbat necesarul caloric. Pe langa aceasta este important sa schimbi meniul pentru a nu interveni plictiseala: trebuie sa multumim si psihicul pe parcursul dietei. De aceea te sfatuiesc ca dupa 4 saptamani sa comanzi un nou meniu. Acela va fi diferit de acesta.

 <br><br><br>

Elena Bodea<br>

Pentru neclaritati puteti sa imi scrieti pe adresa: nutritionistelenab@yahoo.com";

            $email = new Email($config->SenderEmail, $to, "Cabinet Nutritie Elena Bodea – Meniu", $body);
            //  $email->addCC("nutritionistelena@yahoo.com");

            $email->addAttachment($path1);
            $email->send();


////------------------------------------------------------------------------------------------------------------------------------------------

        } else {


            $client->ErrorCode = $errorCode;
            $client->ErrorType = $errorType;
            $client->ErrorMessage = $errorMessage;
        }

        $client->write();

        session_unset();

        header('Content-type: application/xml');
        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        if ($errorCode == 0) {
            echo "<crc>{$errorMessage}</crc>";
        } else {
            echo "<crc error_type=\"{$errorType}\" error_code=\"{$errorCode}\">{$errorMessage}</crc>";
        }


    }


}