<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 2:12 PM
 */

namespace SilverStripe\Nutrition;

use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\AssetAdmin\Forms\UploadField;


class Testimonial extends DataObject
{
    private static $table_name = 'Testimonial';


    private static $db = [
        'Name' => 'Varchar(255)',
        'KG' => 'Varchar(255)',
        'Content' => 'HTMLText',
        'VideoLink' => 'Varchar(255)',


    ];

    private static $has_one = [
        'Image' => Image::class,
        'TestimonialsPage' => TestimonialsPage::class,
    ];

    private static $owns = [
        'Image'
    ];
    private static $summary_fields = [
        "Name" => "Name",
        "KG" => "KG",
        "Content" => "Content"
    ];
    private static $api_access = true;


    public function getCMSFields()
    {
        $fields = FieldList::create(
            TextField::create('Name'),
            TextField::create('KG'),
            HTMLEditorField::create('Content'),
            TextField::create('VideoLink'),
            UploadField::create('Image')

        );

        return $fields;
    }
}