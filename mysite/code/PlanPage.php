<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 1:17 PM
 */
namespace SilverStripe\Nutrition;
use Page;
use PageController;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use View;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;


class PlanPage extends Page
{
    private static $table_name = 'PlanPage';
    private static $has_one = [
        'Background' => Image::class,
        'CenterImage' => Image::class,
        'SideImage' => Image::class,
    ];
    private static $db = [
        'IsForYouText' => 'HTMLText',
        'TreatmentText' => 'HTMLText',
        'ObesityText' => 'HTMLText',
        'InfertilityText' => 'HTMLText',
    ];
    private static $owns = [
        'Background',"CenterImage", "SideImage"
    ];
    private static $has_many = [
        'Steps' => Step::class,

    ];
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldToTab('Root.Main', HTMLEditorField::create('IsForYouText'));
        $fields->addFieldToTab('Root.Main', HTMLEditorField::create('TreatmentText'));
        $fields->addFieldToTab('Root.Main', HTMLEditorField::create('ObesityText'));
        $fields->addFieldToTab('Root.Main', HTMLEditorField::create('InfertilityText'));
        $fields->addFieldToTab('Root.Main', UploadField::create('Background'));
        $fields->addFieldToTab('Root.Main', UploadField::create('CenterImage'));
        $fields->addFieldToTab('Root.Main', UploadField::create('SideImage'));

        $fields->addFieldToTab('Root.Steps', GridField::create(
            'Steps',
            'Steps on this page',
            $this->Steps(),
            GridFieldConfig_RecordEditor::create()
        ));

        return $fields;
    }


}

class PlanPageController extends PageController
{

	
}