<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 1:17 PM
 */
namespace SilverStripe\Nutrition;
use Mobilpay_Payment_Request_Card;
use Mobilpay_Payment_Request_Abstract;
use Page;
use PageController;
use View;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Control\Director;
use SilverStripe\ORM\ArrayList;

//use SilverStripe\ORM\DataList;

//use SilverStripe\ORM\ArrayList;
//use SilverStripe\ORM\ArrayList;

use SilverStripe\View\ArrayData;
//use SilverStripe\CMS\Controllers\ContentController;

class PaymentReturnPage extends Page
{
    private static $table_name = 'PaymentReturnPage';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        return $fields;
    }


}

class PaymentReturnPageController extends PageController
{
    public function index(){
        $orderID = null;
        if(isset($_GET['orderId'])){
            $orderID = $_GET['orderId'];
        }
        $client = Client::get()->filter(["OrderID" => $orderID])->first();
        if($client){
            if($client->Paid){
                return $this->customise([
                    'orderID' => $orderID,

                ]);
            }
        }
        return $this->customise([
            'err' => '1',

        ]);
    }
	
}