<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 1:17 PM
 */
namespace SilverStripe\Nutrition;
use Page;
use PageController;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;

class ThankYouPage extends Page
{
    private static $table_name = 'ThankYouPage';
    private static $has_one = [

    ];
    private static $owns = [

    ];
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        return $fields;
    }


}

class ThankYouPageController extends PageController
{

	
}