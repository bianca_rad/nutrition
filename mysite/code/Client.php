<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 2:12 PM
 */

namespace SilverStripe\Nutrition;

use SilverStripe\Assets\File;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use Page;
use PageController;
use SilverStripe\Assets\Image;
use SilverStripe\ORM\Map;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
use View;

class Client extends DataObject
{
    private static $table_name = 'Client';


    private static $db = [
        'PaymentType' => 'Varchar(255)',

        'OrderID' => 'Varchar(255)',

        'ErrorCode' => 'Varchar(255)',
        'ErrorType' => 'Varchar(255)',
        'ErrorMessage' => 'Varchar(255)',


        'BillingType' => 'Varchar(255)',
        'BillingFirstName' => 'Varchar(255)',
        'BillingLastName' => 'Varchar(255)',
        'BillingAddress' => 'Varchar(255)',
        'BillingEmail' => 'Varchar(255)',
        'BillingPhone' => 'Varchar(255)',

        'Paid' => 'Boolean',

    ];

    private static $has_one = [

        'HomePage' => HomePage::class,
        'FoodProfile' => FoodProfile::class,
        'Document' => File::class,
        'Menu' => File::class,
    ];

    private static $owns = [
'Document',"Menu"
    ];
    private static $summary_fields = [
        "OrderID" => "OrderID",
        "Created" => "Created",
        "PaymentType" => "PaymentType",
        "ErrorCode" => "ErrorCode",
        "Paid" => "Paid",
    ];
    private static $api_access = true;


    /**
     * @return FieldList|static
     */
    public function getCMSFields()
    {


        $fields = FieldList::create(
            TextField::create('PaymentType'),
            TextField::create('OrderID'),
            TextField::create('ErrorCode'),
            TextField::create('ErrorType'),
            TextField::create('ErrorMessage'),
            TextField::create('BillingType'),
            TextField::create('BillingFirstName'),
            TextField::create('BillingLastName'),
            TextField::create('BillingAddress'),
            TextField::create('BillingEmail'),
            TextField::create('BillingPhone'),
            UploadField::create('Document'),
            UploadField::create('Menu'),
            DropdownField::create('FoodProfileID', 'Food Profile', FoodProfile::get()->map('ID', 'ID'))->setEmptyString('(Food profile)'),
            CheckboxField::create('Paid')

        );


        return $fields;
    }
}