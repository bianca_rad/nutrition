<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 1:17 PM
 */
namespace SilverStripe\Nutrition;
use Page;
use PageController;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;

class NewsPage extends Page
{
    private static $table_name = 'NewsPage';
    private static $has_one = [
        'Background' => Image::class
    ];
    private static $owns = [
        'Background'
    ];
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', UploadField::create('Background'));
        return $fields;
    }


}

class NewsPageController extends PageController
{

	
}