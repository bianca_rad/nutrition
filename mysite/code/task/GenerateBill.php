<?php

namespace SilverStripe\Nutrition;

use Page;
use PageController;
use SilverStripe\Assets\Folder;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use View;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\FieldList;
use SilverStripe\Dev\BuildTask;
use SilverStripe\Nutrition\Member;
use SilverStripe\Nutrition\Client;
use SilverStripe\FPDF\FPDF;
use SilverStripe\Control\Email\Email;
use Silverstripe\SiteConfig\SiteConfig;
use SilverStripe\Assets\File;



class GenerateBill extends BuildTask
{

    private static $allowed_actions = array('index');

    protected $title = 'GenerateBill';

    protected $description = 'Generate Bill';

    /**
     * @param \SilverStripe\Control\HTTPRequest $request
     * @throws \SilverStripe\ORM\ValidationException
     */
    public function run($request)
    {

        $config = SiteConfig::current_site_config();
        ob_start();
        require($_SERVER["DOCUMENT_ROOT"] . '/fpdf/fpdf.php');


       // $client = Client::get_by_id("Client", "49");
        $client = Client::get()->filter(["ID" => '49'])->first();


        $nr = ((int)date('Y') - 2017) * 10000 + (int)$config->BillNumber + 1;
        $config->BillNumber = $config->BillNumber + 1;
        $config->write();

        $pdf = new FPDF();
        $pdf->AddPage();

        $pdf->Image('http://nutritie.welldev.ro/public/factura-nutritie.jpg', 0, 0, 210);
        $pdf->SetFont('Arial', '', 10);

        $pdf->SetXY(42, 21.5);
        $pdf->Cell(0, 0, $nr);


        $pdf->SetXY(38, 26.5);
        $pdf->Cell(0, 0, date("d/m/Y", strtotime($client->Created)));

        $pdf->SetXY(74, 26.5);
        $pdf->Cell(0, 0, date("d/m/Y", strtotime($client->Created)));

        $pdf->SetFont('Arial', '', 11);

        $pdf->SetXY(110, 44);
        $pdf->Cell(0, 0, $client->BillingFirstName . " " . $client->BillingLastName);

        $pdf->SetXY(110, 49);
        $pdf->Cell(0, 0, $client->BillingAddress);

        $pdf->SetXY(110, 54);
        $pdf->Cell(0, 0, "Telefon: " . $client->BillingPhone);

        $pdf->SetXY(110, 59);
        $pdf->Cell(0, 0, "Email: " . $client->BillingEmail);


        $pdf->SetFont('Arial', '', 12);


        $pdf->SetXY(140, 75);
        $pdf->Cell(0, 0, "90.00");

        $pdf->SetXY(167, 75);
        $pdf->Cell(0, 0, "90.00");


        $pdf->SetXY(167, 163);
        $pdf->Cell(0, 0, "90.00");

        $pdf->SetXY(187, 177);
        $pdf->Cell(0, 0, "90.00");



        $name = $client->OrderID;
        $path = $_SERVER["DOCUMENT_ROOT"] . "/facturi/" . $name . ".pdf";
        $pdf->Output($path, 'F');




        $file = File::create();

        $file->setFromLocalFile($path, "/facturi/" .  $name . ".pdf");
        $file->Filename =   $name . ".pdf";
        $file->FileFilename = "facturi/" .  $name . ".pdf";
        $file->ParentID = 91;
        $file->write();
        $file->doPublish();

      $client->Document = $file;
      $client->write();










//send to client and admin address
        $to = 'bbiiaa2594@gmail.com';
        $subject = 'PDFs';
        $message = 'pdf';

        $email = new Email('bbiiaa2594@gmail.com', $to, $subject, $message);
        $email->addAttachment($path);
        $email->send();


        ob_end_flush();
    }


}