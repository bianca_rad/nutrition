<?php
/**
 * Created by PhpStorm.
 * User: username
 * Date: 3/9/2018
 * Time: 1:17 PM
 */

namespace SilverStripe\Nutrition;

//use SilverStripe\Control\Director;
use Page;
use PageController;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
use View;
use SilverStripe\Assets\File;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Control\Email\Email;
use Silverstripe\SiteConfig\SiteConfig;

class HomePage extends Page
{
    private static $table_name = 'HomePage';
    private static $db = [

        'IntroEmail' => 'Varchar(255)',
        'IntroText' => 'Varchar(500)',
        'IntroSideText' => 'Varchar(500)',

        'AboutTitle' => 'Varchar(255)',
        'AboutText' => 'HTMLText',

        'FormTitle' => 'Varchar(255)',
        'FormText' => 'HTMLText',

        'TestimonialsBannerTitle' => 'HTMLText',
        'TestimonialsTitle' => 'Varchar(255)',
        'TestimonialsVideo' => 'Varchar(255)',


    ];
    private static $has_one = [
        'Background' => Image::class,
        'AboutImage' => Image::class,
        'TestimonialsImage' => Image::class,
    ];
    private static $owns = [
        'Background', "AboutImage", 'FoodProfile', "Client", "TestimonialsImage"
    ];
    private static $has_many = [
        'FoodProfiles' => FoodProfile::class,
        'Clients' => Client::class,
        'CounterBoxes' => CounterBox::class,
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', UploadField::create('Background'));


        $fields->addFieldToTab('Root.Main', TextField::create('IntroEmail'));
        $fields->addFieldToTab('Root.Main', TextField::create('IntroText'));
        $fields->addFieldToTab('Root.Main', TextField::create('IntroSideText'));

        $fields->addFieldToTab('Root.Main', TextField::create('AboutTitle'));
        $fields->addFieldToTab('Root.Main', HTMLEditorField::create('AboutText'));
        $fields->addFieldToTab('Root.Main', UploadField::create('AboutImage'));


        $fields->addFieldToTab('Root.Main', HTMLEditorField::create('TestimonialsBannerTitle'));
        $fields->addFieldToTab('Root.Main', TextField::create('TestimonialsTitle'));
        $fields->addFieldToTab('Root.Main', UploadField::create('TestimonialsImage'));
        $fields->addFieldToTab('Root.Main', TextField::create('TestimonialsVideo'));

        $fields->addFieldToTab('Root.Main', TextField::create('FormTitle'));
        $fields->addFieldToTab('Root.Main', HTMLEditorField::create('FormText'));
        $fields->addFieldToTab('Root.FoodProfiles', GridField::create(
            'FoodProfiles',
            'Food Profiles on this page',
            $this->FoodProfiles(),
            GridFieldConfig_RecordEditor::create()
        ));
        $fields->addFieldToTab('Root.Clients', GridField::create(
            'Clients',
            'Clients on this page',
            $this->Clients(),
            GridFieldConfig_RecordEditor::create()
        ));
        $fields->addFieldToTab('Root.Boxes', GridField::create(
            'CounterBoxes',
            'CounterBoxes on this page',
            $this->CounterBoxes(),
            GridFieldConfig_RecordEditor::create()
        ));
        return $fields;
    }


}

class HomePageController extends PageController
{


    private static $allowed_actions = [
        'sendProfileForm'
    ];

    public function index()
    {
        if (isset($_GET["err"]) && $_GET["err"]) {
            return $this->customise(["Err" => $_GET["err"]]);
        }
        return $this;
    }

    public function LatestNews()
    {
        return NewsPage::get()->sort("Created DESC")->limit(3);
    }

    public function Testimonials()
    {
        return Testimonial::get()->filter(['VideoLink:not' => null])->limit(3);
    }

    public function ContentTestimonials()
    {
        return Testimonial::get()->filter(['Content:not' => null])->limit(3);
    }

    public function gender()
    {

        $arr = array('1' => 'Masculin', '2' => 'Feminin');
        $arrlist = new ArrayList();
        foreach ($arr as $key => $value) {
            $arrlist->push(
                new ArrayData(array('id' => $key, 'gender' => $value))
            );
        }
        return $arrlist;

    }

    public function activity()
    {

        $arr = array('1' => 'Sedentar', '2' => 'Activ', '3' => 'Foarte Activ');
        $arrlist = new ArrayList();
        foreach ($arr as $key => $value) {
            $arrlist->push(
                new ArrayData(array('id' => $key, 'activity' => $value))
            );
        }
        return $arrlist;

    }

    public function diet()
    {

        $arr = array('1' => 'Vegan', '2' => 'Carnivor', '3' => 'Ovo-lacto-vegetarian', '4' => 'Ovo-vegetarian');
        $arrlist = new ArrayList();
        foreach ($arr as $key => $value) {
            $arrlist->push(
                new ArrayData(array('id' => $key, 'diet' => $value))
            );
        }
        return $arrlist;

    }


    public function sendProfileForm($data)
    {


        $privacy = $data['privacy'];
        $weight = $data['weight'];
        $height = $data['height'];
        $age = $data['age'];
        $activity = $data['activity'];
        $diet = $data['diet'];
        $sex = $data['sex'];
        $email = $data['email'];
        $message = $data['message'];


        $meats = "";
        $meats_ar = array();
        if ($data['meat']) {
            foreach ($data['meat'] as $selected) {
                $meats .= $selected . " ";
                array_push($meats_ar, $selected);
            }
        }


        $fruits = "";
        $fruits_ar = array();
        if ($data['fruit']) {
            foreach ($data['fruit'] as $selected) {
                $fruits .= $selected . " ";
                array_push($fruits_ar, $selected);
            }
        }


        $vegetables = "";
        $vegetables_ar = array();
        if ($data['vegetable']) {
            foreach ($data['vegetable'] as $selected) {
                $vegetables .= $selected . " ";
                array_push($vegetables_ar, $selected);
            }
        }

        $dairys = "";
        $dairys_ar = array();
        if ($data['dairy']) {
            foreach ($data['dairy'] as $selected) {
                $dairys .= $selected . " ";
                array_push($dairys_ar, $selected);
            }
        }

        $others = "";
        $others_ar = array();
        if ($data['others']) {
            foreach ($data['others'] as $selected) {
                $others .= $selected . " ";
                array_push($others_ar, $selected);
            }
        }


        if (!$age || !$email || !$weight || !$height || !$activity || !$diet || !$sex || !$privacy) {
            //  $form->addErrorMessage('YourMessage', 'Completati toate campurile', 'bad');
            return $this->redirectBack();
        }

        $config = SiteConfig::current_site_config();

        $profile = FoodProfile::create();
        $profile->Weight = $weight;
        $profile->Height = $height;
        $profile->Age = $age;
        $profile->Activity = $activity;
        $profile->Diet = $diet;
        $profile->Sex = $sex;
        $profile->Email = $email;
        $profile->Message = $message;

        $profile->Meats = $meats;
        $profile->Vegetables = $vegetables;
        $profile->Fruits = $fruits;
        $profile->Dairys = $dairys;
        $profile->Others = $others;


        $profile->HomePageID = $this->ID;
        $profile->write();


        $_SESSION["foodProfID"] = $profile->ID;


        $service_url = 'http://troy.welldev.ro/commerce-online/commerce/generateMenu';
        $curl = curl_init($service_url);


        $curl_post_data = array(
            //  'weight' => '10',
            'weight' => $weight,
            'height' => $height,
            'age' => $age,
            'sex' => $sex,
            'activity' => $activity,
            'diet' => $diet,
            'meats' => $meats_ar,
            'veges' => $vegetables_ar,
            'fruits' => $fruits_ar,
            'dairies' => $dairys_ar,
            'others' => $others_ar,
            'requestIdentifier' => $profile->ID,
            'email' => $email,
        );


        $data_string = json_encode($curl_post_data);


        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $curl_response = curl_exec($curl);

        if ($curl_response == false || json_decode($curl_response)) {
            $info = curl_getinfo($curl);
            curl_close($curl);

//            $body = json_decode($curl_response)->errormessage->errorCode . " - " . json_decode($curl_response)->errormessage->message . "<br>" . var_export($info);
//            $email = new Email($config->SenderEmail, $config->SenderEmail, "Nutritie - meniu eroare", $body);
//            $email->send();


            $profile->Error = json_decode($curl_response)->errormessage->errorCode . " - " . json_decode($curl_response)->errormessage->message;
            $profile->write();

            return $this->redirect('/?err=' . json_decode($curl_response)->errormessage->errorCode);
        } else {

            $filename = "menu" . $profile->ID; //replace with payment token
            $path = $_SERVER["DOCUMENT_ROOT"] . "/public/assets/menus/" . $filename . ".pdf";
            file_put_contents($path, $curl_response);

            $file = File::create();
            $file->setFromLocalFile($path, "/menus/" . $filename . ".pdf");
            $file->Filename = $filename . ".pdf";
            $file->FileFilename = "menus/" . $filename . ".pdf";
            $file->ParentID = 118;
            $file->write();
            $file->doPublish();


            $_SESSION["fileID"] = $file->ID;


            curl_close($curl);

//die(print_r($config->SenderEmail));

            $body = _t("MenuForm.Greutate", "Greutate: "). " <strong>" . $weight. "</strong><br/>";
            $body .= _t("MenuForm.Inaltime", "Inaltime : ") . "<strong>" . $height. "</strong><br/>";
            $body .= _t("MenuForm.Varsta", "Varsta : ") . " <strong>" . $age. "</strong><br/>";
            $body .= _t("MenuForm.Activitate", "Activitatea fizica: ") . ":  <strong>" . $activity. "</strong><br/>";
            $body .= _t("MenuForm.Dieta", "Dieta: ") . " <strong>" . $diet. "</strong><br/>";
            $body .= _t("MenuForm.Sex", "Sex: ") . "  <strong>" . $sex. "</strong><br/>";
            $body .= _t("MenuForm.Email", "Email: ") . " <strong>" . $email . "</strong><br/>";
            $body .= _t("MenuForm.Mesaj", "Mesaj: ") . " <strong>" . $message . "</strong><br/>";

            $email = new Email($config->SenderEmail, $config->SenderEmail, "Nutritie - meniu formular", $body);
            $email->send();


            return $this->redirect('/card-bill');


        }
    }

}
