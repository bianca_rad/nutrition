<!-- Start #content -->


<!-- Start Slider -->
<section class="section sm-no-padding" data-enable-fullheight="true" style="background-color: #fff;">
    <div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="mainSlider-alt"
         data-source="gallery"
         style="background:#fffcf7;padding:0px;">
        <div id="rev_slider_2_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.5.1">
            <ul>
                <li data-index="rs-20" data-transition="notransition" data-slotamount="default" data-hideafterloop="0"
                    data-hideslideonmobile="off"
                    data-easein="default" data-easeout="default" data-masterspeed="default" data-delay="15000"
                    data-rotate="0" data-saveperformance="off"
                    data-title="Nutritie Main">
                    <img src="/public/resources/themes/simple/images/sliders/main-slider/dummy.png"
                         data-bgcolor="#fff" style="background:#fff" alt=""
                         data-lazyload="/public/resources/themes/simple/images/sliders/main-slider/transparent.png"
                         data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                         data-bgparallax="off" class="rev-slidebg"
                    >
                    <div class="tp-caption   tp-resizeme" id="slide-20-layer-1"
                         data-x="['right','right','right','right']" data-hoffset="['503','503','503','503']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-15','-14','-14','-14']"
                         data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="image" data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1000,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;">
                        <div class="rs-looped rs-wave" data-speed="4" data-angle="80" data-radius="10px"
                             data-origin="50% 50%">
                            <img src="/public/resources/themes/simple/images/sliders/main-slider/dummy.png" alt=""
                                 data-ww="['671px','671px','671px','671px']" data-hh="['792px','792px','792px','792px']"
                                 width="671" height="792"
                                 data-lazyload="/public/resources/themes/simple/images/sliders/main-slider/main-slider-assets1.png">
                        </div>
                    </div>
                    <h3 class="tp-caption   tp-resizeme" id="slide-20-layer-2"
                        data-x="['center','center','center','center']" data-hoffset="['99','199','0','0']"
                        data-y="['middle','middle','middle','middle']" data-voffset="['-229','-178','-178','-178']"
                        data-fontsize="['100','100','100','50']"
                        data-width="['588','588','588','none']" data-height="['85','85','85','none']"
                        data-whitespace="nowrap" data-visibility="['on','on','on','on']"
                        data-type="text" data-responsive_offset="on"
                        data-frames="[{&quot;delay&quot;:580,&quot;split&quot;:&quot;lines&quot;,&quot;splitdelay&quot;:0.4,&quot;speed&quot;:400,&quot;split_direction&quot;:&quot;forward&quot;,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:[-1%];opacity:0;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                        data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                        style="z-index: 6; min-width: 588px; max-width: 588px; max-width: 85px; max-width: 85px; white-space: nowrap; font-size: 100px; line-height: 80px; font-weight: 600; color: rgba(30,18,49,1); font-family:Poppins;letter-spacing:-0.025em;">
                        Nutritionist</h3>
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-20-layer-7"
                         data-x="['center','center','center','center']"
                         data-hoffset="['99','199','0','99']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['-180','-170','-170','-180']"
                         data-width="582" data-height="88" data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']" data-type="shape"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:left;opacity:1;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Quint.easeInOut&quot;},{&quot;delay&quot;:&quot;+30&quot;,&quot;speed&quot;:500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:right;opacity:1;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Quint.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 7;background-color:rgba(30,18,49,1);"></div>
                    <h3 class="tp-caption   tp-resizeme" id="slide-20-layer-36"
                        data-x="['center','center','center','center']"
                        data-hoffset="['-52','48','0','0']" data-y="['middle','middle','middle','middle']"
                        data-voffset="['-122','-71','-71','-111']"
                        data-fontsize="['100','100','100','50']" data-width="none" data-height="none"
                        data-whitespace="nowrap" data-visibility="['on','on','on','on']"
                        data-type="text" data-responsive_offset="on"
                        data-frames="[{&quot;delay&quot;:930,&quot;speed&quot;:400,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:[-1%];opacity:0;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                        data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                        style="z-index: 8; white-space: nowrap; font-size: 100px; line-height: 80px; font-weight: 600; color: rgba(30,18,49,1); font-family:Poppins;letter-spacing:-0.025em;">
                        Elena Bodea </h3>
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-20-layer-38"
                         data-x="['center','center','center','center']"
                         data-hoffset="['-39','61','0','-39']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['-73','-63','-63','-73']"
                         data-width="306" data-height="86" data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']" data-type="shape"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:330,&quot;speed&quot;:500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:left;opacity:1;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Quint.easeInOut&quot;},{&quot;delay&quot;:&quot;+30&quot;,&quot;speed&quot;:500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:right;opacity:1;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Quint.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 9;background-color:rgba(30,18,49,1);"></div>
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-20-layer-30"
                         data-x="['center','center','center','center']"
                         data-hoffset="['101','201','153','153']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['-48','-38','-38','-38']"
                         data-width="17" data-height="17" data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']" data-type="shape"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:1280,&quot;speed&quot;:300,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;sX:0;sY:0;opacity:1;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;+30&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;sX:1.5;sY:1.5;opacity:0;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 11;background-color:rgba(240,48,78,1);border-radius:20px 20px 20px 20px;"></div>
                    <p class="tp-caption" id="slide-20-layer-3"
                       data-x="['center','center','center','center']" data-hoffset="['118','208','0','0']"
                       data-y="['middle','middle','middle','middle']" data-voffset="['44','54','54','4']"
                       data-fontsize="['20','20','20','20']"
                       data-lineheight="['37','37','37','28']" data-width="['620','620','920','920']"
                       data-height="['75','75','75','75']"
                       data-whitespace="['normal','normal','normal','normal']" data-visibility="['on','on','on','on']"
                       data-type="text"
                       data-responsive_offset="on"
                       data-frames="[{&quot;delay&quot;:880,&quot;speed&quot;:800,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:-10px;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                       data-textalign="['inherit','inherit','center','center']" data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                       style="z-index: 12; min-width: 619px; max-width: 619px; max-width: 75px; max-width: 75px; white-space: normal; font-size: 19px; line-height: 37px; font-weight: 300; color: rgba(30,18,49,0.8); font-family:Poppins;">
                        $IntroText
                    </p>

                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-20-layer-50"
                         data-x="['center','center','center','center']" data-hoffset="['108','208','0','108']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['27','37','37','27']" data-width="600" data-height="30" data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"
                         data-type="shape" data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:580,&quot;speed&quot;:450,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:left;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeOut&quot;},{&quot;delay&quot;:&quot;+30&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:right;opacity:1;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power4.easeIn&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 13;background-color:rgba(63,58,71,1);"></div>
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-20-layer-34"
                         data-x="['center','center','center','center']"
                         data-hoffset="['-1','99','0','-1']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['63','73','73','63']"
                         data-width="380" data-height="31" data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']" data-type="shape"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:680,&quot;speed&quot;:450,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:left;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeOut&quot;},{&quot;delay&quot;:&quot;+30&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:right;opacity:1;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power4.easeIn&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 14;background-color:rgba(63,58,71,1);"></div>
                    <div class="tp-caption " id="slide-20-layer-44"
                         data-x="['center','center','center','center']"
                         data-hoffset="['-50','50','-180','0']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['167','177','177','127']"
                         data-fontsize="['17.5','17.5','17.5','17.5']" data-lineheight="['90','90','90','70']"
                         data-color="['rgb(255,255,255)','rgba(255,255,255,1)','rgba(255,255,255,1)','rgba(255,255,255,1)']"
                         data-width="['280','280','280','220']" data-height="['90','90','90','70']"
                         data-whitespace="nowrap" data-visibility="['on','on','on','on']"
                         data-type="text" data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:1230,&quot;speed&quot;:800,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:[100%];opacity:0;&quot;,&quot;mask&quot;:&quot;x:[-100%];y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;frame&quot;:&quot;hover&quot;,&quot;speed&quot;:&quot;250&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;,&quot;to&quot;:&quot;o:1;rX:0;rY:0;rZ:0;z:0;&quot;,&quot;style&quot;:&quot;c:rgb(255,255,255);bg:linear-gradient(135deg, rgba(237,96,35,1) 0%, rgba(255,30,90,1) 100%);-webkit-box-shadow:0 17px 50px rgba(0,0,0,0.1);box-shadow:0 17px 50px rgba(0,0,0,0.1);&quot;}]"
                         data-textalign="['center','center','center','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 15; min-width: 280px; max-width: 280px; max-width: 90px; max-width: 90px; white-space: nowrap; font-size: 17.5px; line-height: 90px; font-weight: 400; color: #ffffff; font-family:Karla;background:linear-gradient(135deg, rgba(244,41,88,1) 0%, rgba(227,67,42,1) 100%);border-radius:3px 3px 3px 3px;letter-spacing:0.1em;cursor:pointer;">
                        <a href="#form-section"
                           style="display: block; width: 100%; height: 100%;color: #fff !important;">
                            <i class="fa fa-play" style="margin-right: 14px; font-size: 11px;"></i>Comanda regimul</a>
                    </div>
                    <div class="tp-caption  underlined" id="slide-20-layer-45"
                         data-x="['center','center','center','center']"
                         data-hoffset="['233','333','180','0']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['167','177','177','200']"
                         data-width="280" data-height="90" data-whitespace="nowrap"
                         data-visibility="['on','on','on','on']" data-type="text"
                         data-actions="[{&quot;event&quot;:&quot;click&quot;,&quot;action&quot;:&quot;scrollbelow&quot;,&quot;offset&quot;:&quot;px&quot;,&quot;delay&quot;:&quot;&quot;,&quot;speed&quot;:&quot;1000&quot;,&quot;ease&quot;:&quot;easeInOutExpo&quot;}]"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:1330,&quot;speed&quot;:800,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:[100%];y:0px;opacity:0;&quot;,&quot;mask&quot;:&quot;x:[-100%];y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;frame&quot;:&quot;hover&quot;,&quot;speed&quot;:&quot;250&quot;,&quot;ease&quot;:&quot;Power0.easeInOut&quot;,&quot;to&quot;:&quot;o:1;skX:0px;rX:0;rY:0;rZ:0;z:0;&quot;,&quot;style&quot;:&quot;c:rgba(0,0,0,1);br:0px 0px 0px 0px;&quot;}]"
                         data-textalign="['center','center','center','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 16; min-width: 280px; max-width: 280px; max-width: 90px; max-width: 90px; white-space: nowrap; font-size: 17.5px; line-height: 90px; font-weight: 400; color: rgba(0,0,0,1); font-family:Karla;text-transform:uppercase;border-radius:3px 3px 3px 3px;letter-spacing:0.1em;cursor:pointer;">
                        Cum comanzi?
                    </div>
                    <span class="tp-caption   tp-resizeme" id="slide-20-layer-46"
                          data-x="['right','right','right','right']"
                          data-hoffset="['-40','-40','-40','-40']" data-y="['bottom','bottom','bottom','bottom']"
                          data-voffset="['144','144','144','144']"
                          data-width="267" data-height="46" data-whitespace="normal"
                          data-visibility="['on','off','off','off']" data-type="text"
                          data-responsive_offset="on"
                          data-frames="[{&quot;delay&quot;:3330,&quot;speed&quot;:800,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:15px;y:0px;sX:1;sY:1;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;rZ:-90;tO:0% 0%;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                          data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                          style="z-index: 17; min-width: 267px; max-width: 267px; max-width: 46px; max-width: 46px; white-space: normal; font-size: 17.5px; line-height: 22px; font-weight: 400; color: rgba(30,18,49,1); font-family:Karla;letter-spacing:0.3em;">
                        $IntroEmail  </span>
                    <div class="tp-caption   tp-resizeme" id="slide-20-layer-47"
                         data-x="['right','right','right','right']"
                         data-hoffset="['168','168','168','168']" data-y="['bottom','bottom','bottom','bottom']"
                         data-voffset="['190','190','190','190']"
                         data-width="none" data-height="none" data-whitespace="normal"
                         data-visibility="['on','off','off','off']" data-type="image"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:3530,&quot;speed&quot;:800,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:15px;y:0px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 18;">
                        <div class="rs-looped rs-wave" data-speed="5" data-angle="0" data-radius="4px"
                             data-origin="50% 50%">
                            <img src="/public/resources/themes/simple/images/sliders/main-slider/dummy.png" alt=""
                                 data-ww="['7px','7px','7px','7px']" data-hh="['73px','73px','73px','73px']"
                                 width="7" height="73"
                                 data-lazyload="/public/resources/themes/simple/images/sliders/main-slider/main-slider-assets2.png">
                        </div>
                    </div>
                    <div class="tp-caption   tp-resizeme" id="slide-20-layer-48"
                         data-x="['right','right','right','right']" data-hoffset="['-218','-218','-218','-218']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['71','71','71','71']"
                         data-width="368" data-height="121"
                         data-whitespace="normal" data-visibility="['on','off','off','off']" data-type="text"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:3730,&quot;speed&quot;:800,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:15px;y:0px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;rZ:-90;tO:0% 0%;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 19; min-width: 368px; max-width: 368px; max-width: 121px; max-width: 121px; white-space: normal; font-size: 16.25px; line-height: 30px; font-weight: 300; color: rgba(30,18,49,1); font-family:Poppins;">
                        $IntroSideText
                    </div>
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-20-layer-59"
                         data-x="['left','left','left','left']" data-hoffset="['430','210','390','240']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['0','0','-295','-230']" data-width="none" data-height="none"
                         data-whitespace="nowrap" data-type="image"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:2360,&quot;speed&quot;:900,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:[-100%];opacity:1;fbr:100;&quot;,&quot;mask&quot;:&quot;x:0px;y:0px;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;fbr:190%;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;+30&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;fbr:100;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-visibility="['on','on','off','off']"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 20;">
                        <img src="/public/resources/themes/simple/images/sliders/main-slider/dummy.png" alt=""
                             data-ww="['177px','177px','115px','94px']" data-hh="['420px','420px','274px','223']"
                             width="177" height="420"
                             data-lazyload="/public/resources/themes/simple/images/sliders/main-slider/main-slider-assets4.png">
                    </div>
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-20-layer-58"
                         data-x="['left','left','left','left']"
                         data-hoffset="['430','210','390','239']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['0','0','-295','-230']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="image"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:2460,&quot;speed&quot;:900,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:[-100%];opacity:1;fbr:100;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;fbr:160%;&quot;,&quot;ease&quot;:&quot;Strong.easeOut&quot;},{&quot;delay&quot;:&quot;+30&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;fbr:100;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-visibility="['on','on','off','off']"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 21;">
                        <img src="/public/resources/themes/simple/images/sliders/main-slider/dummy.png" alt=""
                             data-ww="['177px','177px','115px','94px']" data-hh="['420px','420px','274px','223px']"
                             width="177" height="420"
                             data-lazyload="/public/resources/themes/simple/images/sliders/main-slider/main-slider-assets4.png">
                    </div>
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-20-layer-57"
                         data-x="['left','left','left','left']"
                         data-hoffset="['430','210','390','240']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['0','0','-295','-230']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="image"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:2560,&quot;speed&quot;:900,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:[-100%];opacity:1;fbr:100;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;fbr:140%;&quot;,&quot;ease&quot;:&quot;Strong.easeOut&quot;},{&quot;delay&quot;:&quot;+30&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;fbr:100;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-visibility="['on','on','off','off']"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 22;">
                        <img src="/public/resources/themes/simple/images/sliders/main-slider/dummy.png" alt=""
                             data-ww="['177px','177px','115px','94px']" data-hh="['420px','420px','274px','223px']"
                             width="177" height="420"
                             data-lazyload="/public/resources/themes/simple/images/sliders/main-slider/main-slider-assets4.png">
                    </div>
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-20-layer-56"
                         data-x="['left','left','left','left']"
                         data-hoffset="['430','210','390','239']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['-1','-1','-295','-230']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="image"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:2660,&quot;speed&quot;:900,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:[-100%];opacity:1;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Strong.easeOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-visibility="['on','on','off','off']"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 23;">
                        <img src="/public/resources/themes/simple/images/sliders/main-slider/dummy.png" alt=""
                             data-ww="['177px','177px','115px','94px']" data-hh="['420px','420px','274px','223px']"
                             width="177" height="420"
                             data-lazyload="/public/resources/themes/simple/images/sliders/main-slider/main-slider-assets4.png">
                    </div>
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-20-layer-53"
                         data-x="['left','left','left','left']"
                         data-hoffset="['-150','-370','-193','-340']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['0','0','0','0']"
                         data-width="580" data-height="full" data-whitespace="nowrap" data-type="shape"
                         data-basealign="slide" data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:1030,&quot;speed&quot;:1200,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:left;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power4.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-visibility="['on','on','off','off']"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 24;background-color:rgba(146,15,242,0.85);">
                        <div class="tp-element-background" style="background-size: cover; opacity: 1;"></div>
                    </div>
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-20-layer-54"
                         data-x="['left','left','left','left']"
                         data-hoffset="['-150','-370','-193','-341']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['0','0','0','0']"
                         data-width="580" data-height="full" data-whitespace="nowrap" data-type="shape"
                         data-basealign="slide" data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:1360,&quot;speed&quot;:850,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:left;&quot;,&quot;mask&quot;:&quot;x:0;y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power4.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-visibility="['on','on','off','off']"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 25;background-color:rgba(0,0,0,1);">
                        <div class="tp-element-background"
                             style=" background: url('public/resources/themes/simple/images/sliders/main-slider/main-slider-assets3.jpg') no-repeat left top; background-size: cover; opacity: 1;"></div>
                    </div>
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-20-layer-61"
                         data-x="['left','left','left','left']"
                         data-hoffset="['275','55','288','158']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['0','-1','-295','-230']"
                         data-width="none" data-height="none" data-whitespace="nowrap" data-type="image"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:1830,&quot;speed&quot;:700,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:[100%];&quot;,&quot;mask&quot;:&quot;x:[-100%];y:0;s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Quint.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-visibility="['on','on','off','off']"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 26;">
                        <img src="/public/resources/themes/simple/images/sliders/main-slider/dummy.png" alt=""
                             data-ww="['158px','158px','102px','82px']" data-hh="['420px','420px','270px','218px']"
                             width="158" height="420"
                             data-lazyload="/public/resources/themes/simple/images/sliders/main-slider/main-slider-assets8a.png">
                    </div>
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-20-layer-52"
                         data-x="['left','left','center','center']"
                         data-hoffset="['430','210','0','0']" data-y="['middle','middle','middle','middle']"
                         data-voffset="['0','0','0','0']"
                         data-width="5" data-height="full" data-whitespace="normal" data-type="shape"
                         data-basealign="slide" data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:1890,&quot;speed&quot;:500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;sY:0;opacity:1;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]" data-visibility="['on','on','off','off']"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 27;background-color:rgba(255,255,255,0.85);"></div>
                    <span class="tp-caption   tp-resizeme" id="slide-20-layer-62" data-x="['left','left','left','left']"
                          data-hoffset="['97','97','97','97']"
                          data-y="['bottom','bottom','bottom','bottom']" data-voffset="['112','112','112','112']"
                          data-width="none" data-height="none"
                          data-whitespace="nowrap" data-visibility="['off','off','off','off']" data-type="text"
                          data-actions="[{&quot;event&quot;:&quot;click&quot;,&quot;action&quot;:&quot;scrollbelow&quot;,&quot;offset&quot;:&quot;px&quot;,&quot;delay&quot;:&quot;&quot;,&quot;speed&quot;:&quot;300&quot;,&quot;ease&quot;:&quot;Linear.easeNone&quot;}]"
                          data-responsive_offset="on"
                          data-frames="[{&quot;delay&quot;:3030,&quot;speed&quot;:700,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[50%];opacity:0;&quot;,&quot;to&quot;:&quot;o:1;rZ:-90;tO:0% 0%;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                          data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                          style="z-index: 28; white-space: nowrap; font-size: 17.5px; line-height: 22px; font-weight: 400; color: rgba(255,255,255,1); font-family:Karla;text-transform:uppercase;letter-spacing:0.125em;cursor:pointer;">Explore
									<span class="circle-dark">30</span> Demos </span>
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-20-layer-63"
                         data-x="['left','left','center','center']"
                         data-hoffset="['87','87','0','0']" data-y="['bottom','bottom','bottom','bottom']"
                         data-voffset="['67','67','67','67']"
                         data-width="45" data-height="45" data-whitespace="nowrap" data-type="shape"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:2430,&quot;speed&quot;:900,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:1;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;+30&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 29;background-color:rgba(119,12,218,1);border-radius:45px 45px 45px 45px;"></div>
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-20-layer-64"
                         data-x="['left','left','center','center']"
                         data-hoffset="['87','87','0','0']" data-y="['bottom','bottom','bottom','bottom']"
                         data-voffset="['67','67','67','67']"
                         data-width="45" data-height="45" data-whitespace="nowrap" data-type="shape"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:2630,&quot;speed&quot;:900,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:1;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;+30&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 30;background-color:rgba(205,71,146,1);border-radius:45px 45px 45px 45px;"></div>
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-20-layer-65"
                         data-x="['left','left','center','center']"
                         data-hoffset="['87','87','0','0']" data-y="['bottom','bottom','bottom','bottom']"
                         data-voffset="['67','67','67','67']"
                         data-width="45" data-height="45" data-whitespace="nowrap" data-type="shape"
                         data-actions="[{&quot;event&quot;:&quot;click&quot;,&quot;action&quot;:&quot;scrollbelow&quot;,&quot;offset&quot;:&quot;px&quot;,&quot;delay&quot;:&quot;&quot;,&quot;speed&quot;:&quot;300&quot;,&quot;ease&quot;:&quot;Linear.easeNone&quot;}]"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:2830,&quot;speed&quot;:900,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:1;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 31;background-color:rgba(255,255,255,1);border-radius:45px 45px 45px 45px;cursor:pointer;"></div>
                    <div class="tp-caption   tp-resizeme" id="slide-20-layer-66"
                         data-x="['left','left','center','center']" data-hoffset="['87','87','0','0']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['67','67','67','67']"
                         data-width="45" data-height="45"
                         data-whitespace="nowrap" data-type="text"
                         data-actions="[{&quot;event&quot;:&quot;click&quot;,&quot;action&quot;:&quot;scrollbelow&quot;,&quot;offset&quot;:&quot;px&quot;,&quot;delay&quot;:&quot;&quot;,&quot;speed&quot;:&quot;1000&quot;,&quot;ease&quot;:&quot;easeInOutExpo&quot;}]"
                         data-responsive_offset="on"
                         data-frames="[{&quot;delay&quot;:3230,&quot;speed&quot;:700,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:-10px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]"
                         data-textalign="['center','center','center','center']" data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                         style="z-index: 32; min-width: 45px; max-width: 45px; max-width: 45px; max-width: 45px; white-space: nowrap; font-size: 20px; line-height: 45px; font-weight: 400; color: rgba(0,0,0,1); cursor:pointer;">
                        <div class="rs-looped rs-slideloop" data-easing="easeInExpo" data-speed="0.6" data-xs="0"
                             data-xe="0" data-ys="0"
                             data-ye="5">
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
</section>
<!-- End Slider -->
<hr>
<!-- Start Accordion And Lightbox -->
<section class="about-section section pt-100 pb-100 ">
    <div class="container">
        <div class="row">

            <div class="d-none d-lg-block col-lg-4">
                <img src="$AboutImage.URL" alt="">
            </div>
            <div class="col-lg-7 offset-lg-1 col-md-12">
                <header class="section-title section-title-default section-title-resolve align-left mb-25"
                        data-plugin-resolve="true">
                    <h2>
                        $AboutTitle
                    </h2>
                </header>
                $AboutText
                <div class="accordion accordion-underline accordion-right mb-40" id="accordion-59f9a7ae365a1"
                     role="tablist" aria-multiselectable="true">
                    <div class=" accordion-item">
                        <h4 class="accordion-toggle">
                            <a data-toggle="tab"> 1.Evaluare initiala</a>
                        </h4>

                    </div>
                    <div class=" accordion-item">
                        <h4 class="accordion-toggle">
                            <a data-toggle="tab">2.Dieta personalizata</a>
                        </h4>
                    </div>
                    <div class=" accordion-item">
                        <h4 class="accordion-toggle">
                            <a data-toggle="tab"> 3.Monitorizare si ajustare </a>
                        </h4>
                    </div>
                    <div class=" accordion-item">
                        <h4 class="accordion-toggle">
                            <a data-toggle="tab"> 4.Consultatii saptamanale </a>
                        </h4>
                    </div>
                </div>


                <a class="btn btn-linear btn-xlg round text-uppercase icon-left "
                   href="#form-section">
								<span>
									<i class="novi-icon fa fa-play"></i>Comanda Regimul</span>
                </a>
                <a class="btn  btn-xlg round text-uppercase icon-left " target="_blank"
                   href="/plan">
								<span>
									<i class="novi-icon fa fa-play"></i>Afla mai multe</span>
                </a>
            </div>


        </div>
    </div>
</section>


<hr>
<!-- Start form -->
<section class="section pt-80 pb-10 form-section" id="form-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <header class="section-title section-title-default align-center mb-30">
                    <h2>$FormTitle  </h2>
                    <h6>$FormText</h6>

                    <% if $Err ==1 %>
                        <div class="menu-error">No valid entry data. Check form attributes.</div>
                    <% end_if %>
                    <% if $Err ==2 %>
                        <div class="menu-error">No need to lose weight.</div>
                    <% end_if %>
                    <% if $Err ==3 %>
                        <div class="menu-error">Unable to generate the menu for this combination of disliked ingredients. Try another
                            combination.
                        </div>
                    <% end_if %>
                </header>
            </div>
            <div class="col-sm-12">
                <div class="contact-form contact-default contact-default-primary">
                    <form method="post"
                          id="profileForm"
                          enctype="multipart/form-data"
                          action="{$Link}sendProfileForm"
                          data-form-output="form-output-global" data-form-type="forms"
                    >

                        <input type="hidden" name="SecurityID" value="$SecurityID">

                        <div class="row">


                            <p class="col-lg-3 col-md-6">
                                <label for="weight">
                                    <span><b>Greutate</b> (kg)</span>
                                </label>
                                <span class="form-group">
												<input class="form-control" type="text" name="weight" id="weight"
                                                       aria-required="true" aria-invalid="false">
											</span>

                            </p>
                            <p class="col-lg-3 col-md-6">
                                <label for="height">
                                    <span><b>Inaltime</b> (cm)</span>
                                </label>
                                <span class="form-group">
												<input class="form-control" type="text" name="height" id="height"
                                                       aria-required="true" aria-invalid="false">
											</span>

                            </p>
                            <p class="col-lg-3 col-md-6">
                                <label for="age">
                                    <span><b>Varsta</b> (ani)</span>
                                </label>
                                <span class="form-group">
												<input class="form-control" type="text" name="age" id="age"
                                                       aria-required="true" aria-invalid="false">
											</span>

                            </p>
                            <p class="col-lg-3 col-md-6">
                                <label for="sex">
                                    <span><b>Sex</b></span>
                                </label>
                                <select name="sex" id="sex"
                                        aria-invalid="false">

                                    <% loop $gender %>

                                    <option value="$id">$gender<label for=""></label>
                                    <% end_loop %>


                                </select>
                            </p>

                            <p class="col-lg-4 col-md-6">

<span class="form-group">    <label for="activity">
                                    <span>Cum va descrieti <b>activitatea fizica</b> ?</span>
                                </label>
												<select name="activity" id="activity"
                                                        aria-invalid="false">

                                                    <% loop $activity %>

                                                    <option value="$id">$activity<label for=""></label>
                                                    <% end_loop %>


                                                </select>
											</span>

                            </p>


                            <p class="col-lg-3 col-md-6">

<span class="form-group">
    <label for="diet">
                                    <span><b>Tip dieta</b></span>
                                </label>

												<select name="diet" id="diet"
                                                        aria-invalid="false">
                                                </select>
											</span>

                            </p>
                            <p class="col-lg-5 col-md-12">
                                <label for="email">
                                    <span><b>Email</b></span>
                                </label>
                                <span class="form-group">
												<input class="form-control" type="email" name="email" id="email"
                                                       aria-required="true" aria-invalid="false">
											</span>

                            </p>


                            <br>
                            <div class="form-group col-12">
                                <span><b>Ingrediente nepreferate:</b></span> <br>
                                <small>Bifeaza alimentele pe care NU le vrei in meniul tau
                                </small>
                            </div>
                            <hr>
                            <div class="form-group col-12">
                                <div class="checkbox-alert ckbox-alert-meat">
                                    <span>Alegeti maxim 3 ingrediente din aceasta categorie</span>
                                    <span class="alert-close">×</span>
                                </div>

                                <span><b>Carne (max. 3 ingrediente)</b></span><br>

                                <div id="meats-wrap"></div>

                            </div>
                            <hr>

                            <div class="form-group col-12">
                                <div class="checkbox-alert ckbox-alert-vegetable">
                                    <span>Alegeti maxim 3 ingrediente din aceasta categorie</span>
                                    <span class="alert-close">×</span>
                                </div>

                                <span><b>Legume (max. 3 ingrediente)</b></span> <br>
                                <div id="veges-wrap"></div>

                            </div>
                            <hr>


                            <div class="form-group  col-12">
                                <div class="checkbox-alert ckbox-alert-fruit">
                                    <span>Alegeti maxim 3 ingrediente din aceasta categorie</span>
                                    <span class="alert-close">×</span>
                                </div>
                                <span><b>Fructe (max. 3 ingrediente)</b></span><br>
                                <div id="fruits-wrap"></div>

                            </div>

                            <hr>

                            <div class="form-group  col-12">
                                <div class="checkbox-alert ckbox-alert-dairy">
                                    <span>Alegeti maxim 3 ingrediente din aceasta categorie</span>
                                    <span class="alert-close">×</span>
                                </div>
                                <span><b>Lactate (max. 3 ingrediente)</b></span><br>
                                <div id="dairies-wrap"></div>


                            </div>

                            <hr>


                            <span class="form-group  col-12">
                                 <div class="checkbox-alert ckbox-alert-others">
                                         <span>Alegeti maxim 3 ingrediente din aceasta categorie</span>
                                    <span class="alert-close">×</span>
                                </div>
                            <span><b>Altele (max. 3 ingrediente)</b></span>
                              <br>
                           <span id="others-wrap"></span>


                            </span>


                            <br>
                            <hr>
                            <br>
                            <p class="col-sm-12">
                                <label for="message">
                                    <span><b>Mesaj/Observatii</b></span>
                                </label>
                                <span class="form-group">
												<textarea class="form-control" name="message" id="message" cols="40"
                                                          rows="6" aria-required="true" aria-invalid="false"></textarea>
											</span>

                            </p>


                            <p class="col-sm-12">
                                <span class="privacy-wrap"><label><input type="checkbox" name="privacy" value="1"
                                                                            aria-invalid="false"><span
                                        class="">Accept <a
                                        href="/privacy-policy">termenii și condițiile </a></span></label></span>


                                <span class="clearfix"></span>


                                <span class="right"><input type="submit"
                                                           value="<%t HomePage.Card_Payment 'Plateste cu card' %>"
                                                           name="card_payment" class="button medium gray submit-btn"
                                                           data-loading-text="Loading...">

                                </span>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End form -->

<hr>


<section class="section testimonials-banner-section" style="background-image: none; opacity: 1; visibility: visible;"
         data-parallax-bg="true">
    <figure class="parallax-img-parent">
        <div class="parallax-img-container"><img class="section-parallax-img"
                                                 src="$TestimonialsImage.URL"><img
                class="parallax-img-placeholder"
                src="$TestimonialsImage.URL"></div>
    </figure>
    <div class="container section-parallax-img">
        <div class="row align-items-stretch">

            <div class="col-lg-6 d-flex">
                <div class="lightbox-video flex-column">

                    <figure>

                        <div class="hover-line">
                            <i class="icon fa fa-play"></i>
                            <span>Testimonial Video</span>
                            <div class="line left"></div>
                            <div class="line right"></div>
                        </div>

                        <a href="$TestimonialsVideo" class="lightbox-link"
                           data-type="video"></a>

                    </figure>

                </div><!-- /.lightbox-video -->

            </div> <!-- /.col-lg-6 -->

            <div class="col-sm-8 offset-sm-4 col-md-6 offset-md-6 col-lg-4 offset-lg-2 pt-100 pb-130">

                <header class="section-title section-title-default mb-40 text-left is-visible"
                        data-plugin-resolve="true"
                        data-plugin-resolve-options="{&quot;element&quot;:&quot;h2&quot;,&quot;seperator&quot;:&quot;chars&quot;,&quot;startDelay&quot;:0}">


                    $TestimonialsBannerTitle

                </header>

                <a href="#" class="btn btn-linear btn-xlg round text-uppercase icon-left ">
                    <span>Vezi mai multe  <i class="fa fa-angle-right"></i>	</span>
                </a>

            </div> <!-- /.col-lg-6 -->

        </div> <!-- /.row -->
    </div> <!-- /.container -->

</section>

<section class="section pt-50 pb-10">
    <div class="container">
        <div class="row">
            <div class="col">
                <header class="section-title section-title-default align-center">
                    <h2>     $TestimonialsTitle </h2>
                </header>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 mx-auto col-lg-8">
                <div class="carousel-container carousel-nav-style10"
                     data-flickity-options="{&quot;cellAlign&quot;:&quot;center&quot;}">
                    <div class="carousel-items">
                        <% loop $ContentTestimonials %>

                            <div class="col-sm-12">
                                <div class="testimonial testimonial-minimal-vertical testimonial-minimal-vertical-big text-center">

                                    <div class="testimonial-details">
                                        <span class="testimonial-name">$Name</span>
                                    </div>
                                    <span class="quote-symbol"></span>
                                    <blockquote class="testimonial-quote">
                                        $Content
                                    </blockquote>
                                </div>
                            </div>

                        <% end_loop %>
                    </div>
                    <div class="carousel-nav">
                        <button class="flickity-prev-next-button previous">
                            <i class="fa fa-angle-left"></i>
                        </button>
                        <button class="flickity-prev-next-button next">
                            <i class="fa fa-angle-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section pt-70 pb-70">
    <div class="container">
        <div class="row">


            <% loop $Testimonials %>
                <div class="col-lg-4 col-md-6 mx-auto">
                    <div class="content-box content-box-img content-box-img-default --red text-center">
                        <figure>
                            <img src="$Image.URL" alt="Testimonial">
                            <a href="$VideoLink"
                               class="btn btn-naked text-uppercase lightbox-link" data-type="video">
										<span>Vezi video
											<i class="fa fa-long-arrow-right"></i>
										</span>
                            </a>
                        </figure>
                        <div class="content-box-content">
                            <h3 class="text-uppercase">$Name <br>- $KG Kilograme slabite</h3>
                        </div>
                    </div>
                </div>
            <% end_loop %>


        </div>
    </div>
</section>


<!-- Start Counters -->
<section class="section pt-30 pb-30">
    <div class="container-fluid">
        <div class="row">
            <% loop $CounterBoxes %>
                <div class="col-md-4 px-0">
                    <div class="counter-box counter-box-sep counter-box-xxlg2 text-center text-uppercase"
                         data-plugin-counter="true">
                        <div class="contents">
                            <p class="weight-bold">$Title</p>
                            <h3 class="counter-element">
                                <span class="weight-medium">$Number</span>
                            </h3>
                        </div>
                    </div>
                </div>
            <% end_loop %>

        </div>
    </div>
</section>
<!-- End Counters -->


<!-- Start Contact Info -->
<section class="section pt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-10 ml-auto mr-auto">
                <h4 class="mb-40 color-gray">Ne gasesti la</h4>
                <div class="tabs tabs-title-naked">
                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation" class="active" data-plugin-resolve="true"
                            data-plugin-resolve-options='{ "element": "a", "seperator": "chars", "startDelay": 0.6 }'>
                            <a href="#tab-title-naked-5832d82d0f411" aria-controls="tab-title-naked-5832d82d0f411"
                               role="tab" data-toggle="tab"
                               aria-selected="true">Cabinet Nutritionist Elena Bodea.</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-title-naked-5832d82d0f411" aria-labelledby="tab-title-naked-5832d82d0f411"
                             role="tabpanel" class="tab-pane fade show active">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="weight-normal color-gray">Trimite un email!</h4>
                                    <h4 class="weight-normal color-black">nutritionistelenab@yahoo.com</h4>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="weight-normal color-gray">Ma gasesti aici:</h4>
                                    <h4 class="weight-normal color-black">Strada Mircea Stanescu nr 2,
                                        <br>Arad, Romania</h4>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="weight-normal color-gray">Comanda acum!</h4>
                                    <%--<h4 class="weight-normal color-black">0754556252</h4>--%>
                                </div>
                                <div class="col-md-6 mt-30">
                                    <ul class="social-icon social-icon-lg scheme-gray">
                                        <li>
                                            <a href="https://www.facebook.com/teajutsaslabesti/s" target="_blank">
                                                <i class="novi-icon fa fa-facebook"></i> Urmareste pe Facebook
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Contact Info -->


<!-- Start Call To Action -->
<section class="section pt-50 sm-no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 no-padding text-center">
                <a href="#form-section" class="btn btn-linear btn-block cta-big">
                    <span>Comanda acum!</span>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Call To Action -->


<!-- End #content -->


<div class="snackbars" id="form-output-global"></div>




<style>
    .menu-error {

        padding: 30px;
        font-size: 20px;
        color:#fff;
        background-color: #ff4c3b;
        z-index: 999;
    }

</style>